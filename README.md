# TIMB033

All of my creations from the ``TIMB033`` course right here in one place!✨

> Hmm, coding is sometimes fun.

## [Art A - Letterism](https://gitlab.fi.muni.cz/xtokaro1/timb033/-/tree/main/a)

![Déja vu](/a/results/a-TokarovaHana-DejaVu.gif "Déja vu")
